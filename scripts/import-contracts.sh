#!/usr/bin/env bash

pwd
cp ../contracts/build/contracts/Token.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/Identity.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/Trollbox.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/TrollboxProxy.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/ChainLinkOracle.json ./src/truffle/build/contracts/
cp ../contracts/build/contracts/ReferralProgram.json ./src/truffle/build/contracts/
