#!/usr/bin/env bash
HOME=$(pwd)
cd ../contracts/
truffle deploy
cd $HOME
./scripts/import-contracts.sh
