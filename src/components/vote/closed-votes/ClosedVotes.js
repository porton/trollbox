import { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Ballot from 'components/vote/ballot'
import './ClosedVotes.scss'

function ClosedVotes ({ className, ballots }) {
  const resolveBallots = useMemo(() => (
        ballots && ballots
          .sort((ballot1, ballot2) => {
            const value1 = ballot1.number
                , value2 = ballot2.number

            if (value1 < value2) {
              return 1
            }

            if (value1 > value2) {
              return -1
            }

            return 0
          })
      ), [ ballots ])
      , hasBallots = useMemo(() => (
        resolveBallots && resolveBallots.length > 0
      ), [ resolveBallots ])

  return (
    <Container
      className={classnames('closed-votes', className)}
      fluid
    >
      <Row>
        <Col>
          <h1>Closed Votes</h1>
        </Col>
      </Row>
      <Row>
        {hasBallots
          ? (
            resolveBallots.map(({
              number,
              startDate,
              endDate,
              resultsDate,
              projectedWinner,
              marketWinner,
              votes,
              voteSum,
              voteMax
            }, index) => (
              <Col
                key={`ballot${index}`}
                xs={12}
                md={6}
              >
                <Ballot
                  className={classnames(
                    'closed-votes__ballot',
                    `m${index % 2 ? 'l' : 'r'}-4`
                  )}
                  ballotNumber={number}
                  startDate={startDate}
                  endDate={endDate}
                  resultsDate={resultsDate}
                  projectedWinner={projectedWinner}
                  marketWinner={marketWinner}
                  votes={votes}
                  voteSum={voteSum}
                  voteMax={voteMax}
                />
              </Col>
            ))
          )
          : (
            <Col>No available votes</Col>
          )
        }
      </Row>
    </Container>
  )
}

ClosedVotes.propTypes = {
  className: PropTypes.string,
  ballots: PropTypes.arrayOf(PropTypes.shape({
    number: PropTypes.number.isRequired,
    startDate: PropTypes.number.isRequired,
    endDate: PropTypes.number.isRequired,
    resultsDate: PropTypes.number.isRequired,
    projectedWinner: PropTypes.object.isRequired,
    marketWinner: PropTypes.object,
    votes: PropTypes.array.isRequired
  }))
}

export default memo(ClosedVotes)
