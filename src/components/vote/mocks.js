import CompIcon from 'assets/icons/comp.png'
import LinkIcon from 'assets/icons/link.png'
import LendIcon from 'assets/icons/lend.png'
import SnxIcon from 'assets/icons/snx.png'
import YfiIcon from 'assets/icons/yfi.png'
import AmplIcon from 'assets/icons/ampl.png'

const mocks = {
  projectedWinner: {
    icon: CompIcon,
    name: 'Compound'
  },
  marketWinner: {
    icon: LendIcon,
    name: 'Aave'
  },
  votes: [
    {
      icon: CompIcon,
      name: 'Compound',
      symbol: 'COMP',
      price: {
        symbol: '$',
        value: 167.73,
        difference: 0.79
      }
    }, {
      icon: LinkIcon,
      name: 'Chainlink',
      symbol: 'LINK',
      price: {
        symbol: '$',
        value: 15.13,
        difference: 0.79
      }
    }, {
      icon: LendIcon,
      name: 'Aave',
      symbol: 'LEND',
      price: {
        symbol: '$',
        value: 0.631,
        difference: -2.48
      }
    }, {
      icon: SnxIcon,
      name: 'Syncthetic Network',
      symbol: 'SNX',
      price: {
        symbol: '$',
        value: 6.18,
        difference: -0.89
      }
    }, {
      icon: YfiIcon,
      name: 'yearn.finance',
      symbol: 'YFI',
      price: {
        symbol: '$',
        value: 13047.90,
        difference: 3.26
      }
    }, {
      icon: AmplIcon,
      name: 'Ampleforth',
      symbol: 'AMPL',
      price: {
        symbol: '$',
        value: 0.776,
        difference: 1.32
      }
    }
  ]
}

export default mocks
