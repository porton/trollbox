import renderer from 'react-test-renderer'
import VoteFormProgressBar from './VoteFormProgressBar'

test('should match snapshot', () => {
  const tree = renderer.create(<VoteFormProgressBar />).toJSON()

  expect(tree).toMatchSnapshot()
})
