import { useState, useEffect } from 'react'
import Select from 'components/form/select'
import { InputNumber, Button } from 'antd'
import Card from 'react-bootstrap/Card'
import { resolveRound } from 'utils/simpleSetters'
import { getCurrentRoundId, getTournament, getWinnerChainlink } from 'utils/simpleGetters'
import { getHistoricChainlinkPrices } from 'utils/eventGetters'
import logos from 'tests/mocks/asset-list'

function ResolveRound () {
  const [ roundToResolve, setRoundToResolve ] = useState(0)
      , [ winningOption, setWinningOption ] = useState(0)
      , [ lastPossibleResolved, setLastPossibleResolved ] = useState(0)
      // , [ tournament, setTournament ] = useState({})
      , [ options, setOptions ] = useState([])
      , tournamentId = 1

  function updateRound (roundId) {
    if (roundId > 0) {
      setRoundToResolve(roundId)
    }
  }

  useEffect(() => {
    const f = async () => {
      const roundId = await getCurrentRoundId(tournamentId)
          , lastRound = roundId - 2

      setLastPossibleResolved(lastRound)
    }

    f()
  }, [])

  useEffect(() => {
    const f = async () => {
      const tournament = await getTournament(tournamentId)
          , options = tournament.tokenList.map(({ name, price }, index) => ({
            key: index + 1,
            value: index + 1,
            label: `${name}  ${price.difference}`
          }))

      // setTournament(tournament)
      setOptions(options)
      await getWinnerChainlink()
    }

    f()
  }, [ roundToResolve ])

  return (
    <Card>
      <Card.Body>
        <Card.Title>Resolve Round</Card.Title>
        <InputNumber style={{}} min={1} max={lastPossibleResolved} defaultValue={lastPossibleResolved} onChange={updateRound} />
        <Select
          className="vote-page__identity-control"
          options={options}
          onChange={x => setWinningOption(x.value)}
        />
        <Button type={'primary'} onClick={() => resolveRound(tournamentId, roundToResolve, winningOption) }>
          Submit
        </Button>
      </Card.Body>
    </Card>
  )
}

export default ResolveRound
