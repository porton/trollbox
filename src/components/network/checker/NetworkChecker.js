import { memo, useState, useEffect, useCallback, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Alert from 'react-bootstrap/Alert'
import ethereum, { metamaskWrongNetwork } from 'utils/ethereum'
import './NetworkChecker.scss'

function NetworkChecker ({ className }) {
  const [ currentNetwork, setCurrentNetwork ] = useState(null)
      , [ show, setShow ] = useState(false)

      , checkNetwork = useCallback(async () => {
        try {
          const { isOk, current, expected } = await metamaskWrongNetwork()

          if (isOk) {
            console.warn(`NetworkChecker: detected connection to ${current}`)
          } else {
            console.warn(`NetworkChecker: wrong network. Expected ${expected}, detected ${current}`)
          }

          setShow(!isOk)
          setCurrentNetwork(current)
        } catch (err) {
          console.error(err)

          setShow(true)
        }
      }, [])

  useEffect(() => {
    checkNetwork()

    ethereum.addEventListener(ethereum.EVENTS.NETWORK_CHANGE, checkNetwork)

    return () => {
      ethereum.removeEventListener(ethereum.EVENTS.NETWORK_CHANGE, checkNetwork)
    }
  }, [ checkNetwork ])

  if (show) {
    return (
      <Alert
        className={classnames('network-checker', className)}
        variant="warning"
        dismissible
        onClose={() => setShow(false)}
      >
        Opps, looks like we cannot connect to the blockchain.
        Your current network is {currentNetwork || 'unknown'}.
        <br />
        Check which network your ethereum wallet is connected to and refresh the page.
      </Alert>
    )
  }

  return <Fragment />
}

NetworkChecker.propTypes = {
  className: PropTypes.string
}

export default memo(NetworkChecker)
