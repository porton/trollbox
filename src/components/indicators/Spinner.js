import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import BSSpinner from 'react-bootstrap/Spinner'
import './Spinner.scss'

function Spinner (props) {
  const spinnerProps = { ...props }
      , { overlay } = props

  delete spinnerProps.overlay

  return (
    <div className={classnames('spinner', { 'spinner--overlay': overlay })}>
      <BSSpinner animation="border" role="status" {...spinnerProps}>
        <span className="sr-only">Loading...</span>
      </BSSpinner>
    </div>
  )
}

Spinner.propTypes = {
  overlay: PropTypes.bool
}

export default memo(Spinner)
