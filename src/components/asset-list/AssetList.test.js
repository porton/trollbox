import renderer from 'react-test-renderer'
import AssetList from './AssetList'

test('should match snapshot', () => {
  const tree = renderer.create(<AssetList />).toJSON()

  expect(tree).toMatchSnapshot()
})
