import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import numeral from 'numeral'
import Container from 'react-bootstrap/Container'
import ProgressBar from 'react-bootstrap/ProgressBar'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import './AssetListItem.scss'

function AssetListItem ({ className, icon, name, symbol, price, children, progress, daysBack = 7 }) {
  return (
    <Container
      className={classnames('asset-list-item', className)}
    >
      {typeof progress !== 'undefined' && (
        <Row className="asset-list-item__volume">
          <Col>
            <ProgressBar now={progress} />
          </Col>
        </Row>
      )}
      <Row>
        <Col className="align-self-center" xs="auto">
          <img className="asset-list-item__icon" src={icon} alt="" />
        </Col>
        <Col>
          <Row>
            <Col className="text-left">
              <div className="asset-list-item__name">{name}</div>
              <div className="asset-list-item__symbol">{symbol}</div>
            </Col>
            {price && (
              <Col className="text-right" xs="auto">
                <div className="asset-list-item__price">{numeral(price.value).format(`${price.symbol} 0,0.00`)}</div>
                <div
                  className={classnames(
                    'asset-list-item__price-difference',
                    `asset-list-item__price-difference--${price.difference > 0 ? 'positive' : 'negative'}`
                  )}>
                  {numeral(price.difference).format('+ 0,0.00')}%
                  &nbsp;
                  <span className="text-muted">{daysBack}d</span>
                </div>
              </Col>
            )}
          </Row>
        </Col>
        {children}
      </Row>
    </Container>
  )
}

AssetListItem.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired,
  price: PropTypes.shape({
    symbol: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    difference: PropTypes.number.isRequired
  }),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  progress: PropTypes.number
}

export default memo(AssetListItem)
