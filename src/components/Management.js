import { memo } from 'react'
// import {
//   setSiteHash,
//   updateTournament,
//   setWinnerOracle,
//   setTrollboxManagement,
//   createTournament,
//   setIdentityManagement,
//   createIdentityFor,
//   setIdentityPriceIncreaseFactor,
//   setIdentityPriceDecayFactor,
//   setIdentityPriceFloor
// } from 'utils/fin.js'

import { Button } from 'antd'

const setTrollboxManagement = () => console.log('Mock function, setTrollboxManagement is not implemented')
    , setSiteHash = () => console.log('Mock function, setSiteHash is not implemented')
    , updateTournament = () => console.log('Mock function, updateTournament is not implemented')
    , setWinnerOracle = () => console.log('Mock function, setWinnerOracle is not implemented')
    , createTournament = () => console.log('Mock function, createTournament is not implemented')
    , setIdentityManagement = () => console.log('Mock function, setIdentityManagement is not implemented')
    , createIdentityFor = () => console.log('Mock function, createIdentityFor is not implemented')
    , setIdentityPriceIncreaseFactor = () => console.log('Mock function, setIdentityPriceIncreaseFactor is not implemented')
    , setIdentityPriceDecayFactor = () => console.log('Mock function, setIdentityPriceDecayFactor is not implemented')
    , setIdentityPriceFloor = () => console.log('Mock function, setIdentityPriceFloor is not implemented')

function Management ({ callback }) {
  return (
    <div>
      <Button type={'primary'} onClick={setTrollboxManagement} > Set Trollbox Management </Button>
      <Button type={'primary'} onClick={setSiteHash} > Set Site Hash </Button>
      <Button type={'primary'} onClick={updateTournament} > Update Tournament </Button>
      <Button type={'primary'} onClick={setWinnerOracle} > Set Winner Oracle </Button>
      <Button type={'primary'} onClick={createTournament} > Create Tournament </Button>

      <Button type={'primary'} onClick={setIdentityManagement} > Set Identity Management </Button>
      <Button type={'primary'} onClick={createIdentityFor} > Create Identity For </Button>
      <Button type={'primary'} onClick={setIdentityPriceIncreaseFactor} > Set Identity Price Increase Factor </Button>
      <Button type={'primary'} onClick={setIdentityPriceDecayFactor} > Set Identity Price Decay Factor </Button>
      <Button type={'primary'} onClick={setIdentityPriceFloor} > Set Identity Price Floor </Button>

    </div>
  )
}

export default memo(Management)
