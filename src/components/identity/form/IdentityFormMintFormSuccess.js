import { memo } from 'react'
import PropTypes from 'prop-types'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import { NavLink } from 'react-router-dom'

function IdentityFormMintFormSuccess ({ ownedTokens, mintAnotherCallback }) {
  return (
    <div>
      <Form.Row>
        <Col>
          <p>Success! You now own</p>
          <div className="form-control text-center">{'FVT ' + ownedTokens}</div>
        </Col>
      </Form.Row>
      <Form.Row>
        <Col>
          <Button variant="secondary" onClick={mintAnotherCallback}>Mint another identity</Button>
        </Col>
      </Form.Row>
      <Form.Row>
        <Col>
          <NavLink
            to="/vote"
            variant="primary"
            component={Button}
          >
            Start voting
          </NavLink>
        </Col>
      </Form.Row>
    </div>
  )
}

IdentityFormMintFormSuccess.propTypes = {
  ownedTokens: PropTypes.number
}

IdentityFormMintFormSuccess.defaultProps = {
  ownedTokens: 0
}

export default memo(IdentityFormMintFormSuccess)
