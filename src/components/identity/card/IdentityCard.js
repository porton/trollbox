import { memo, useMemo, Fragment, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card, { CARD_VARIANTS } from 'components/containers/card'
import { formatNumber } from 'utils/formats'
import IdentityMarketCard from './market-card'
import Ballot from 'components/vote/ballot'
import Spinner from 'components/indicators/Spinner'
import { useClosedVoteBallots } from 'queries/token'
import { getVotesByIdentity } from 'utils/eventGetters'
import { getVoiceCredits } from 'utils/simpleGetters'
import './IdentityCard.scss'

function IdentityCard ({ className, id, fvtBalance, voteMarkets }) {
  const hasVoteMarkets = useMemo(() => voteMarkets?.length > 0, [ voteMarkets ])
      , [ tournamentId ] = useState(1)
      , [ votes, setVotes ] = useState([])
      , [ currentV, setCurrentV ] = useState(0)
      , [ selectedBallotNum, setSelectedBallotNum ] = useState(0)
      , [ voteMax, setVoteMax ] = useState(0)
      , selectedBallot = useClosedVoteBallots(tournamentId, [ selectedBallotNum ])
      , [ selectedBallotVotes, setSelectedBallotVotes ] = useState([])
      , [ voteEvents, setVoteEvents ] = useState({})
      , changeBallotNum = (value) => {
        console.log({ selectedBallotNum })
        setSelectedBallotNum(value)
        setVotes(voteEvents[value])
      }

  useEffect(() => {
    const updateVoteEvents = (newEvent) => {
      setVoteEvents(prevVoteEvents => { return { ...prevVoteEvents, [newEvent.roundId]: newEvent } })
    }

    getVotesByIdentity((voteEvent) => {
      updateVoteEvents(voteEvent)
    }, id)

    const updateV = async () => {
      const v = await getVoiceCredits(tournamentId, id)
      setCurrentV(v)
    }

    updateV()
  }, [ id ])

  useEffect(() => {
    if (selectedBallot.data && votes.choices) {
      const votesResult = selectedBallot.data[0].votes.map((vote, index) => {
        const choice = votes.choices.indexOf((index + 1).toString())

        if (choice > -1) {
          vote.weight = votes.weights[choice]
        } else {
          vote.weight = 0
        }
        return vote
      })
      const max = Math.max(...votesResult.map(x => x.weight))

      setVoteMax(max)
      setSelectedBallotVotes(votesResult)
    }
  }, [ selectedBallot, votes ])

  return (
    <Row>
      <Col xs="6">
        <Card
          className={classnames('identity-card', className)}
          variant={CARD_VARIANTS.DARK}
        >
          <Container fluid>
            <Row className="align-items-end">
              <Col className="identity-card__title" xs="5">
                DeFi Winners
              </Col>
              <Col className="identity-card__balance">
                $V Power <span title={currentV}>{formatNumber(currentV)}</span>
              </Col>
              <Col className="identity-card__balance">
                $FVT Balance <span title={fvtBalance}>{formatNumber(fvtBalance)}</span>
              </Col>
            </Row>
            {hasVoteMarkets && (
              <Fragment>
                <Row className="identity-card__vote-markets">
                  <Col>
                    <Container className="identity-card__vote-markets__container" fluid>
                      <Row>
                        <Col>
                          {Object.values(voteEvents).map((vote, index) => (
                            <IdentityMarketCard
                              key={`identity-market-card${index}`}
                              name={`#${vote.roundId}`}
                              rewards={vote.winnings}
                              showBallot={changeBallotNum}
                              roundId={parseInt(vote.roundId)}
                              voterId={id}
                              className={vote.roundId == selectedBallotNum ? 'selected' : ''}
                            />
                          ))}
                        </Col>
                      </Row>
                    </Container>
                  </Col>
                </Row>
              </Fragment>
            )}
          </Container>
        </Card>
      </Col>
      <Col xs="6">
        { selectedBallot.isLoading
          ? <Spinner />
          : selectedBallotNum > 0 && selectedBallot.data &&
          <Ballot
            className="closed-votes__ballot"
            ballotNumber={selectedBallot.data[0].number}
            startDate={selectedBallot.data[0].startDate}
            endDate={selectedBallot.data[0].endDate}
            resultsDate={selectedBallot.data[0].resultsDate}
            marketWinner={selectedBallot.data[0].marketWinner}
            votes={selectedBallotVotes}
            voteMax={voteMax}
            showCompletePendingIcon={false}
            ballotLabel="Your votes"
            sort={true}
          />
        }
      </Col>
    </Row>
  )
}

IdentityCard.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  fvtBalance: PropTypes.number,
  voteMarkets: PropTypes.array
}

IdentityCard.defaultProps = {
  fvtBalance: 0
}

export default memo(IdentityCard)
