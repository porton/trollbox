import renderer from 'react-test-renderer'
import IdentityMarketCard from './IdentityMarketCard'

test('should match snapshot', () => {
  const tree = renderer.create(<IdentityMarketCard />).toJSON()

  expect(tree).toMatchSnapshot()
})
