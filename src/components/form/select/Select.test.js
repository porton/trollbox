import renderer from 'react-test-renderer'
import Select from './Select'

test('should match snapshot', () => {
  const tree = renderer.create(<Select />).toJSON()

  expect(tree).toMatchSnapshot()
})
