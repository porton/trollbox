import { memo, useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import numeral from 'numeral'
import Form from 'react-bootstrap/Form'

const KEY_UP = 38
    , KEY_DOWN = 40
    , format = '-0[.][000000000000000000]'

function NumberControl (props) {
  const controlRef = useRef()
      , { value, min, max, onChange } = props
      , [ formattedValue, setFormattedValue ] = useState(formatValue(value))
      , [ prevFormattedValue, setPrevFormattedValue ] = useState(formatValue(value))

  function formatValue (value) {
    if (!value || value === '0') {
      return ''
    }

    const formattedValue = numeral(Math.max(min, Math.min(value, max))).format(format)

    if (/\.$/.test(value)) {
      const parts = value.toString().match(/\.\d*$/)

      return `${formattedValue}${parts.length > 0 ? parts[0] : '.'}`
    }

    return formattedValue
  }

  function handleFocus () {
    controlRef.current.select()
  }

  function handleChange ({ target: { value } }) {
    setFormattedValue(formatValue(value))
  }

  function handleKeyDown ({ keyCode }) {
    let value = parseFloat(formattedValue) || 0

    switch (keyCode) {
      case KEY_UP:
        value++
        break
      case KEY_DOWN:
        value--
        break
      default:
        break
    }

    setFormattedValue(formatValue(value))
  }

  useEffect(() => {
    if (prevFormattedValue !== formattedValue) {
      onChange && onChange(formattedValue)
      setPrevFormattedValue(formattedValue)
    }
  }, [ prevFormattedValue, formattedValue, onChange ])

  useEffect(() => {
    const newFormattedValue = formatValue(value)

    if (newFormattedValue !== formattedValue) {
      setFormattedValue(newFormattedValue)
    }
  }, [ value ])

  return (
    <Form.Control
      {...props}
      ref={controlRef}
      type="text"
      placeholder="0"
      value={formattedValue}
      onFocus={handleFocus}
      onChange={handleChange}
      onKeyDown={handleKeyDown}
    />
  )
}

NumberControl.propTypes = {
  className: PropTypes.string,
  value: PropTypes.number
}

export default memo(NumberControl)
