import { useEffect } from 'react'
import { Select, Statistic, Button } from 'antd'
import Loader from 'components/containers/loader'
import {
  useIdentitiesForAccount,
  useIdentityPrice,
  useCreateIdentity
} from 'queries/token'

const { Option } = Select

function IdentitySelector ({ callback }) {
  const identities = useIdentitiesForAccount()
      , identityPrice = useIdentityPrice()
      , [ createIdentity, createdIdentity ] = useCreateIdentity()

  useEffect(() => {
    const timeout = setTimeout(identityPrice.refetch, 1000)
        , interval = setInterval(identityPrice.refetch, 15000)

    return () => {
      clearTimeout(timeout)
      clearInterval(interval)
    }
  }, [ identities.data, identityPrice.refetch ])

  return (
    <div>
      <Loader
        isLoading={identityPrice.isLoading || createdIdentity.isLoading}
        error={identityPrice.error || createdIdentity.error}
      >
        <Statistic
          title={'Identity Price (FVT)'}
          value={identityPrice.data}
          precision={3}
        />
        <Loader
          isLoading={identities.isLoading}
          error={identities.error}
        >
          <Select
            defaultValue="Select Identity"
            style={{ width: 120 }}
            onChange={callback}
          >
            {identities.data.map(x => (
              <Option value={x.tokenId} key={x.txHash}>{x.tokenId}</Option>
            ))}
          </Select>
        </Loader>
        <Button
          type={'primary'}
          onClick={() => createIdentity(identityPrice.data)}
        >
          Create Identity
        </Button>
      </Loader>
    </div>
  )
}

export default IdentitySelector
