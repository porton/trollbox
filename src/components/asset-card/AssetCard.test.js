import renderer from 'react-test-renderer'
import AssetCard from './AssetCard'

test('should match snapshot', () => {
  const tree = renderer.create(<AssetCard />).toJSON()

  expect(tree).toMatchSnapshot()
})
