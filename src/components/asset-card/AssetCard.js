import { memo, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import moment from 'moment'
import './AssetCard.scss'

export const AssetCardTypes = {
  PROJECTED: 'projected',
  PENDING: 'pending',
  COMPLETED: 'completed'
}

function formatDate (date) {
  return moment.unix(date).format('DD/MM/YY')
}

function AssetCard ({ className, type, title, asset, date }) {
  let typedClassName

  switch (type) {
    case AssetCardTypes.PROJECTED:
      typedClassName = 'asset-card--projected'
      break
    case AssetCardTypes.PENDING:
      typedClassName = 'asset-card--pending'
      break
    case AssetCardTypes.COMPLETED:
      typedClassName = 'asset-card--completed'
      break
    default:
      break
  }

  return (
    <div className={classnames(
      'asset-card',
      typedClassName,
      className
    )}>
      <div className="asset-card__title">{title}</div>
      <div className="asset-card__label">
        {asset
          ? (
            <Fragment>
              <img className="asset-card__label__icon" src={asset.icon} alt="" />
              {asset.name}
            </Fragment>
          )
          : (
            <Fragment>
              TBA {formatDate(date)}
            </Fragment>
          )
        }
      </div>
    </div>
  )
}

AssetCard.propTypes = {
  className: PropTypes.string,
  type: PropTypes.oneOf(Object.values(AssetCardTypes)),
  title: PropTypes.string,
  asset: PropTypes.shape({
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }),
  date: PropTypes.number
}

export default memo(AssetCard)
