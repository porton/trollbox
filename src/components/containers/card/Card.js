import { memo } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './Card.scss'

export const CARD_VARIANTS = {
  LIGHT: 'light',
  DARK: 'dark'
}

function Card ({ className, variant, children }) {
  return (
    <div
      className={classnames(
        'card',
        { 'card--dark': variant === CARD_VARIANTS.DARK },
        className
      )}
    >
      {children}
    </div>
  )
}

Card.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ])
}

Card.defaultProps = {
  variant: CARD_VARIANTS.LIGHT
}

export default memo(Card)
