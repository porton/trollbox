import { memo, useMemo, useEffect, useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Select from 'components/form/select'
import IdentityCard from 'components/identity/card'
import Loader from 'components/containers/loader'
import { useIdentitiesForAccount } from 'queries/token'
import { getIdentitiesForAccount, getVotesByIdentity } from 'utils/eventGetters'
import { claimPowerAllRounds } from 'utils/simpleSetters'
import './Account.scss'

function Account ({ className }) {
  const identitiesForAccount = useIdentitiesForAccount()
      , [ identities, setIdentities ] = useState(null)
      , [ selectedIdentity, setSelectedIdentity ] = useState(null)
      , [ votesForSelectedIdentity, setVotesForSelectedIdentity ] = useState(null)
      , [ selectedBallot, setSelectedBallot ] = useState(null)
      , identitySelectableOptions = useMemo(() => (
        identities
          ? Object.keys(identities).map(identity => ({
            value: identity,
            label: `FVT ${identity}`
          }))
          : []
      ), [ identities ])
      , defaultSelectableIdentity = useMemo(() => (
        identitySelectableOptions && identitySelectableOptions.find(({ value }) => value === selectedIdentity.tokenId.toString())
      ), [ identitySelectableOptions, selectedIdentity ])
      , claimAll = () => {
        console.log({ identitiesForAccount })
        // dedupe identities array TODO: why are there duplicates?
        const uniqueIds = identitiesForAccount.data.filter((item, pos, self) => self.indexOf(item) == pos)

        claimPowerAllRounds(uniqueIds)
      }

      , getIdentities = useCallback(async () => {
        console.log('getIdentities')

        try {
          await getIdentitiesForAccount(transferEvent => {
            setIdentities(identities => {
              const identity = transferEvent

              if (!selectedIdentity && identity) {
                setSelectedIdentity(identity)
              }
              return {
                ...(identities || {}),
                [identity.tokenId.toString()]: identity
              }
            })
          })
        } catch (err) {
          // setGetIdentitiesFailed(true) // TODO consider changing this to a better connection checker
          console.error(err)
        }
      }, [])

  useEffect(() => {
    getIdentities()
  }, [])

  function handleIdentityChange ({ value }) {
    setSelectedIdentity(identities[value])
  }

  return (
    <div className={classnames('account', className)}>
      <Loader
        isLoading={identitiesForAccount.isLoading}
        error={identitiesForAccount.error}
      >
        <Container fluid>
          <Row className="account__menu">
            <Col>
              <Row className="account__title">
                <div>
                  My Account /
                </div>
                <Col>
                  <Select
                    className="account__identity-control"
                    options={identitySelectableOptions}
                    value={defaultSelectableIdentity}
                    onChange={handleIdentityChange}
                  />
                </Col>
              </Row>
            </Col>
            <Col className="account__claim-power">
              <Button variant="primary" onClick={() => claimAll()}>Claim All Power</Button>
            </Col>
          </Row>
          <Row>
            <Col>
              {selectedIdentity
                ? (
                  <IdentityCard
                    key={`identity-card-${selectedIdentity.tokenId}`}
                    className="account__identity-card"
                    id={selectedIdentity.tokenId}
                    fvtBalance={selectedIdentity.fvtBalance}
                    voteMarkets={selectedIdentity.voteMarkets}
                  />
                )
                : (
                  <div>You do not have any identities associated with this account</div>
                )
              }
            </Col>
          </Row>
        </Container>
      </Loader>

    </div>
  )
}

Account.propTypes = {
  className: PropTypes.string
}

export default memo(Account)
