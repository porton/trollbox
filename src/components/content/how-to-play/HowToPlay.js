import Item from './item'
import Badge from './icons/badge.svg'
import Checked from './icons/checked.svg'
import Group from './icons/group.svg'
import Search from './icons/search.svg'
import Vote from './icons/vote.svg'
import './HowToPlay.scss'

function HowToPlay () {
  return (
    <div className="how-to-play">
      <h3 className="how-to-play__title">How to Play</h3>
      <div>
        <Item
          icon={Group}
          label="Select an identity"
        />
        <Item
          icon={Search}
          label="Research the projects in the token list"
        />
        <Item
          icon={Vote}
          label="Spend your budget of vote power ($V), everyone starts with 100 $V. Every vote costs the square of the vote number e.g. 5 votes = 5 x 5 = 25 $V"
        />
        <Item
          icon={Checked}
          label="Submit your vote"
        />
        <Item
          icon={Badge}
          label="Wait till the vote “End Date”. If your token wins, you will a share of the reward pool proportional to the number of your votes."
        />
      </div>
    </div>
  )
}

export default HowToPlay
