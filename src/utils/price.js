import { getReadContractByAddress } from './ethereum.js'
import { getHistoricChainlinkPricesWrapper } from './eventGetters'
import oldPrices from '../oldBallotsPrice.json'

import AggregatorV3Interface from '../truffle/build/contracts/AggregatorV3Interface.json'

const symbolToAddressUSD = {
      AAVE: '0xF2d87E37EA1e54c7Aa913d2447A5f69f61C114Cf',
      LINK: '0x8cDE021F0BfA5f82610e8cE46493cF66AC04Af53',
      DAI: '0xFe16F630Eb0Ca70661B071360701abf980126d3e',
      UNI: '0x5B0e9Ff11aae806067787d380967900551919c0D',
      COMP: '0x150631a2e822d3ed7D46df9A270ce7134a16De89',
      YFI: '0xAec0D77fdD6a2a34EC3eaF915260496Ae27f9D25',
      SNX: '0xC8DB8d5869510Bb1FCd3Bd7C7624c1b49c652ef8',
      REN: '0xD286AF227B7b0695387E279B9956540818B1dc2a',
      WBTC: '0xF570deEffF684D964dc3E15E1F9414283E3f7419',
      ETH: '0x00c7A37B03690fb9f41b5C5AF8131735C7275446',
      MKR: '0x0309B42c1DC9Ed9d95c06Aa32646Eea1Ca232d95',
      UMA: '0x8e674727e7e53bdAbBC9C0Dc844a36a2163e6c6A'
    }
    , roundSymbolToUSDPrice = {
      1: {
        ETH: 0
      },
      live: {}
    }
    , fvtRoundIdToChainlinkRoundId = {
      1: 0,
      2: 0,
      3: 0
    }
    , chainlinkDecimals = {

    }

export async function getChainlinkDecimals (symbol) {
  if (chainlinkDecimals[symbol]) {
    return chainlinkDecimals[symbol]
  } else {
    const contract = await getReadContractByAddress(AggregatorV3Interface, symbolToAddressUSD[symbol])
        , decimals = (await contract.decimals()).toNumber()
    chainlinkDecimals[symbol] = decimals
    return decimals
  }
}

// this function called in parallel for performance
async function getChainlinkPrice (symbol) {
  if (!roundSymbolToUSDPrice.live) {
    roundSymbolToUSDPrice.live = {
      [symbol]: 0
    }
  } else if (roundSymbolToUSDPrice.live[symbol]) {
    return roundSymbolToUSDPrice.live[symbol]
  }

  let price = 0

  if (symbolToAddressUSD[symbol]) {
    const contract = await getReadContractByAddress(AggregatorV3Interface, symbolToAddressUSD[symbol])
        , roundData = await contract.latestRoundData()
        , decimals = await getChainlinkDecimals(symbol)

    console.log('live price for ', symbol, roundData.answer.toString())
    price = roundData.answer.toString() / (10 ** decimals)
  } else {
    console.warn('Unknown symbol', symbol)
  }

  roundSymbolToUSDPrice.live[symbol] = price
  return price
}

async function getLivePrices (tokens) {
  const prices = {}
      , lastRoundNum = Object.keys(oldPrices.ballots).reduce((a, b) => parseInt(a) > parseInt(b) ? a : b)
      , deferred = []

  for (const x in tokens) {
    deferred.push(getChainlinkPrice(tokens[x].symbol))
  }

  const usdPrices = await Promise.all(deferred)

  let i = 0

  for (const x in tokens) {
    prices[tokens[x].symbol] = { value: usdPrices[i] }
    prices[tokens[x].symbol].difference = (prices[tokens[x].symbol].value - oldPrices.ballots[lastRoundNum][tokens[x].symbol].value) / oldPrices.ballots[lastRoundNum][tokens[x].symbol].value * 100
    i++
  }

  return prices
}

export async function getPrices (roundNum, tokens) {
  console.log('getPrices', roundNum, tokens)

  let prices = {}

  if (roundNum === 'live') {
    prices = await getLivePrices(tokens)
  } else if (oldPrices.ballots[roundNum] !== undefined) {
    prices = oldPrices.ballots[roundNum]
    // console.log('getting prices from oldBallots', prices)
  } else if (roundSymbolToUSDPrice[roundNum]) {
    prices = roundSymbolToUSDPrice[roundNum]
    // console.log('getting prices from memo', prices)
  } else {
    // console.log('getting historic chainlink prices', roundNum)
    const previousPrices = await getPrices(roundNum - 1, tokens)

    // console.log('got previous prices', roundNum, previousPrices)
    prices = await getHistoricChainlinkPricesWrapper(roundNum)

    //    console.log('got historic chainlink prices', prices)
    tokens.forEach(x => {
      prices[x.symbol].difference = (prices[x.symbol].value - previousPrices[x.symbol].value) * 100 / previousPrices[x.symbol].value
    })
    // console.log('computed differences', prices)
    roundSymbolToUSDPrice[roundNum] = prices
  }

  return prices
}
