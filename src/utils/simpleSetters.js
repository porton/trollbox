import {
  getContract,
  getEthAccount,
  toWei,
  fromWei
} from './ethereum.js'
import { getClaimablePowerAll } from './simpleGetters'

import Trollbox from 'truffle/build/contracts/Trollbox.json'
import TrollboxProxy from 'truffle/build/contracts/TrollboxProxy.json'
import FVT from 'truffle/build/contracts/Token.json'
import Identity from 'truffle/build/contracts/Identity.json'
import ReferralProgram from 'truffle/build/contracts/ReferralProgram.json'

const emptyHash = '0x0000000000000000000000000000000000000000000000000000000000000000'

export async function updateAccount (voterId, tournamentId, roundId) {
  const trollbox = await getContract('write', Trollbox)
      , account = await getEthAccount()

  await trollbox.updateAccount(voterId, tournamentId, roundId, { from: account, gas: 130000 })
}

export async function createIdentityFor (addr) {
  const identity = await getContract('write', Identity)
      , account = await getEthAccount()

  await identity.createIdentityFor(addr, { from: account })
}

export async function resolveRound (tournamentId, roundId, winningOption) {
  const trollbox = await getContract('write', Trollbox)
      , account = await getEthAccount()

  await trollbox.resolveRound(tournamentId, roundId, winningOption, { from: account })
}

export async function submitVote (voterId, tournamentId, choices, weights, updateRoundId = 0) {
  const contract = await getContract('write', Trollbox)
      , account = await getEthAccount()

  console.log('vote', voterId, tournamentId, choices, weights, updateRoundId)
  // 1 vote costs < 150k, 10 votes costs 550k
  // 400k / 9 ~= 45k => gasPrice = 100k + 45k * choices.length
  const gasLimit = 100000 + (50000 * choices.length)

  console.log('submitVote', { voterId, tournamentId, choices, weights, updateRoundId })
  return await contract.vote(voterId, tournamentId, choices, weights, emptyHash, updateRoundId, { from: account, gas: gasLimit })
}

export async function createIdentity (maxPrice, referer) {
  const identity = await getContract('write', Identity)
      , fvt = await getContract('write', FVT)
      , referral = await getContract('write', ReferralProgram)
      , account = await getEthAccount()
      , currentApproval = await fvt.allowance(account, identity.address)

  console.log({ currentApproval, maxPrice })
  console.log('currentApproval', currentApproval.toString(), fromWei(currentApproval.toString()))
  console.log({ referer })
  if (parseFloat(fromWei(currentApproval.toString())) < parseFloat(maxPrice)) {
    await fvt.approve(identity.address, toWei(maxPrice), { from: account, gas: 50000 })
    console.log('finished approving')
  }
  console.log('createMyIdentity')
  const rcpt = await identity.createMyIdentity(toWei(maxPrice), { from: account, gas: 150000 })
      , ditId = rcpt.logs[1].args.token.toNumber()

  if (referer > 0) {
    console.log('setReferral')
    // referer first, referee second
    await referral.setReferral(referer, ditId, { from: account, gas: 50000 })
  }

  return ditId
}

export async function claimPowerAllRounds (voterIds) {
  const trollboxProxy = await getContract('write', TrollboxProxy)
      , account = await getEthAccount()
      , tournamentIds = [ 1 ]
      , packages = {
        voterIds: [],
        tournamentIds: [],
        roundIds: []
      }

  console.log('voterIds', voterIds)
  for (let j = 0; j < voterIds.length; j++) {
    const voterId = voterIds[j].tokenId

    for (let i = 0; i < tournamentIds.length; i++) {
      const tournamentId = tournamentIds[i]
          , roundIdObj = await getClaimablePowerAll(voterId, tournamentId)

      roundIdObj.unclaimedRounds.forEach(roundId => {
        packages.voterIds.push(voterId)
        packages.tournamentIds.push(tournamentId)
        packages.roundIds.push(roundId)
      })
    }
  }

  console.log('voterIds', voterIds)
  packages.voterIdsWithTokens = voterIds.filter(x => x.unclaimedTokens > 0).map(x => x.tokenId)
  packages.voterIdsWithTokens.push(...packages.voterIds)
  packages.voterIdsWithTokens = packages.voterIdsWithTokens.filter((el, i, arr) => arr.indexOf(el) === i)
  //  packages.voterIdsWithTokens = packages.voterIds.filter((el, i, arr) => arr.indexOf(el) === i)

  const gasLimit = (130000 * packages.voterIds.length) + (55000 * packages.voterIdsWithTokens.length) + 15000

  console.log('claimPower', Object.values(packages))
  await trollboxProxy.updateAndWithdraw(...Object.values(packages), { from: account, gas: gasLimit })
}

export async function claimPowerOneRound (voterId, roundId) {
  const trollboxProxy = await getContract('write', TrollboxProxy)
      , account = await getEthAccount()
      , tournamentIds = [ 1 ]
      , voterIds = [ voterId ]
      , roundIds = [ roundId ]
      , gasLimit = 130000 + 55000 + 15000

  await trollboxProxy.updateAndWithdraw(voterIds, tournamentIds, roundIds, voterIds, { from: account, gas: gasLimit })
}
