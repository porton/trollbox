import { useState, useEffect } from 'react'
import { useQuery, useMutation } from 'react-query'
import logos from 'tests/mocks/asset-list'
import {
  getVoteTotals,
  getCurrentRoundId,
  getIdentityPrice,
  getTokenBalance,
  getTournament,
  getTokenList,
  getDaysBack,
  getBallotForRound,
  getNumIdentities
} from 'utils/simpleGetters'
import { getIdentitiesForAccount } from 'utils/eventGetters'
import {
  createIdentity,
  submitVote
} from 'utils/simpleSetters'
import { getPrices } from 'utils/price'

function useQueryWithCallback (action) {
  const [ isLoading, setIsLoading ] = useState(true)
      , [ error, setError ] = useState(null)
      , [ data, setData ] = useState([])
      , [ dataHandler, handleData ] = useState(null)

  useEffect(() => {
    try {
      action(newData => {
        setIsLoading(false)
        setData(data => [ newData, ...data ])
        dataHandler && dataHandler(newData)
      })
    } catch (err) {
      setError(err)
    }
  }, [ action, dataHandler ])

  return {
    isLoading,
    error,
    data,
    handleData
  }
}

export const useNumIdentities = () => useQuery('numIdentities', () => getNumIdentities())

export const useIdentitiesForAccount = () => useQueryWithCallback(getIdentitiesForAccount)

export const useCurrentRoundId = tournamentId => useQuery('currentRoundId', () => getCurrentRoundId(tournamentId))

export const useIdentityPrice = () => useQuery('identityPrice', getIdentityPrice)

export const useTokenBalance = () => useQuery('tokenBalance', getTokenBalance)

export const useCreateIdentity = () => useMutation(createIdentity)

export const useSubmitVote = () => useMutation(variables => submitVote(...Object.values(variables)))

export const useClosedVoteBallots = (tournamentId, roundIds) =>
  useQuery(
    [ 'closedVoteBallots', { tournamentId, roundIds } ],
    async () => {
      console.log('useClosedVoteBallots', roundIds)
      if (!roundIds || roundIds.length === 0) {
        console.error(`Invalid roundIds, expected array, got ${roundIds}`)

        return
      }
      const tournament = await getTournament(tournamentId)
      const ballots = []

      for (let i = 0; i < roundIds.length; i++) {
        const roundId = roundIds[i]
        ballots.push(await getBallotForRound(tournament, roundId))
      }
      return ballots
    }
  )
