import Comp from 'assets/icons/comp.png'
import Dai from 'assets/icons/dai.png'
import Link from 'assets/icons/link.png'
import Snx from 'assets/icons/snx.png'
import Yfi from 'assets/icons/yfi.png'
import Wbtc from 'assets/icons/wbtc.png'
import Mkr from 'assets/icons/mkr.png'
import Ren from 'assets/icons/ren.png'
import Uma from 'assets/icons/uma.png'
import Uni from 'assets/icons/uni.png'

const logos = {
  comp: Comp,
  dai: Dai,
  link: Link,
  snx: Snx,
  yfi: Yfi,
  wbtc: Wbtc,
  mkr: Mkr,
  uma: Uma,
  uni: Uni,
  ren: Ren
}
//  {
//    icon: Ant,
//    name: 'Aragon',
//    symbol: 'ANT',
//    price: {
//      symbol: '$',
//      value: 6.96,
//      difference: 15.32
//    }
//  }

export default logos
