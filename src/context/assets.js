/* eslint import/no-anonymous-default-export: [2, {"allowArrowFunction": true}] */

import { createContext, useContext } from 'react'

export const AssetsContext = createContext({ foo: 1 })

export default () => useContext(AssetsContext)
