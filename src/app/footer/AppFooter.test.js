import renderer from 'react-test-renderer'
import AppFooter from './AppFooter'

test('should match snapshot', () => {
  const tree = renderer.create(<AppFooter />).toJSON()

  expect(tree).toMatchSnapshot()
})
