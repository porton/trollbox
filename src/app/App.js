import { useState, useEffect, lazy, Suspense } from 'react'
import { HashRouter, Switch, Route, useLocation, useHistory } from 'react-router-dom'
import NetworkChecker from 'components/network/checker'
import Spinner from 'components/indicators/Spinner'
import ErrorBoundry from 'utils/ErrorBoundry'
import Header from './header'
import Footer from './footer'
import './App.scss'

const Home = lazy(() => import('pages/home'))
    , Account = lazy(() => import('pages/account'))
    , HowItWorks = lazy(() => import('pages/how-it-works'))
    , Identity = lazy(() => import('pages/identity'))
    , Management = lazy(() => import('pages/management'))
    , Vote = lazy(() => import('pages/vote'))

function App () {
  const [ showVoteHowTo, setVoteHowTo ] = useState(true)
      , { pathname } = useLocation()
      , history = useHistory()

  useEffect(() => {
    switch (pathname) {
      case '/vote':
        if (showVoteHowTo) {
          history.push('/vote/how-it-works')
        }
        break
      case '/vote/how-it-works':
        setVoteHowTo(false)
        break
      default:
        break
    }
  }, [ showVoteHowTo, pathname, history ])

  return (
    <HashRouter>
      <div className="app">
        <NetworkChecker className="app__network-checker" />
        <Header className="app__header" />
        <div className="app__body">
          <ErrorBoundry>
            <Suspense fallback={<Spinner />}>
              <Switch>
                <Route path="/account" component={Account} exact />
                <Route path="/identity" component={Identity} exact />
                <Route path="/management" component={Management} exact />
                <Route path="/vote" component={Vote} exact />
                <Route path="/vote/how-it-works" component={HowItWorks} exact />
                <Route path="/" component={Home} />
              </Switch>
            </Suspense>
          </ErrorBoundry>
        </div>
        <Footer className="app__footer" />
      </div>
    </HashRouter>
  )
}

export default App
