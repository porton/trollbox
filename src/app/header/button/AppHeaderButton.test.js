import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import AppHeaderButton from './AppHeaderButton'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <AppHeaderButton />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})
