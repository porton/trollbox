import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import AppHeader from './AppHeader'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <AppHeader />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})
