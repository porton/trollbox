import { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router } from 'react-router-dom'
import { ReactQueryCacheProvider, QueryCache } from 'react-query'
import App from 'app'
import ErrorBoundry from 'utils/ErrorBoundry'
import reportWebVitals from './reportWebVitals'
import './index.scss'

const queryCache = new QueryCache()

ReactDOM.render(
  <StrictMode>
    <ErrorBoundry>
      <ReactQueryCacheProvider queryCache={queryCache}>
        <Router>
          <App />
        </Router>
      </ReactQueryCacheProvider>
    </ErrorBoundry>
  </StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
