import React from 'react'
import renderer from 'react-test-renderer'
import { MemoryRouter } from 'react-router-dom'
import HowItWorksPage from './HowItWorksPage'

test('should match snapshot', () => {
  const tree = renderer.create((
    <MemoryRouter>
      <HowItWorksPage />
    </MemoryRouter>
  )).toJSON()

  expect(tree).toMatchSnapshot()
})
