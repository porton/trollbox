import { memo, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Page from 'pages/Page'
import Form from 'components/identity/form'
import Button from 'react-bootstrap/Button'
import './IdentityPage.scss'

function IdentityPage ({ className, children }) {
  return (
    <Page
      className={classnames('identity-page', className)}
      title="Mint your finance.vote identity"
      leftContent={(
        <Fragment>
          <p>
            In order to mint an identity, you need to burn
            some $FVT. This is a sybil protection
            mechanism, a key component of our
            decentralised voting system.
          </p>
          <p>
            Then you can vote in our voting markets. If you
            vote correctly, you gain more voting power.
            <br />
            <strong>And voting is all about power.</strong>
          </p>
          <a href="https://app.uniswap.org/#/swap?outputCurrency=0x45080a6531d671ddff20db42f93792a489685e32&inputCurrency=ETH" target="_blank" rel="noreferrer">
            <Button variant="primary">Purchase $FVT</Button>
          </a>
        </Fragment>
      )}
      rightContent={(
        <Form />
      )}
    />
  )
}

IdentityPage.propTypes = {
  className: PropTypes.string
}

export default memo(IdentityPage)
