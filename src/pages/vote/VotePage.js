import { memo, useState, useEffect, useCallback, useMemo, Fragment } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import ProgressBar from 'react-bootstrap/ProgressBar'
import Form from 'components/vote/form'
import Select from 'components/form/select'
import BoxedValue from 'components/containers/boxed-value'
import Loader from 'components/containers/loader'
import Page from 'pages/Page'
import { useCurrentRoundId } from 'queries/token'
import { formatDate, formatDateShort } from 'utils/formats'
import logos from 'tests/mocks/asset-list'
import {
  getTournament,
  getVoiceCredits,
  getClaimablePowerAll,
  getDaysBack
} from 'utils/simpleGetters'
import { getPrices } from 'utils/price'
import { getIdentitiesForAccount } from 'utils/eventGetters'
import { connectWallet } from 'utils/ethereum'
import { Statistic } from 'antd'
import './VotePage.scss'

const { Countdown } = Statistic
    , tournamentId = 1

function VotePage ({ className }) {
  const [ tournament, setTournament ] = useState(null)
      , [ roundEnd, setRoundEnd ] = useState(0)
      , [ payoutDate, setPayoutDate ] = useState(null)
      , [ startDate, setStartDate ] = useState(null)
      , [ endDate, setEndDate ] = useState(null)
      , [ roundBonus, setRoundBonus ] = useState(0)
      , [ vPerRound, setVPerRound ] = useState(0)
      , [ identities, setIdentities ] = useState(null)
      , [ selectedIdentity, setSelectedIdentity ] = useState(null)
      , [ getIdentitiesFailed, setGetIdentitiesFailed ] = useState(false)
      , [ updateRound, setUpdateRound ] = useState(0)
      , [ roundProgress, setRoundProgress ] = useState(0)

      , roundId = useCurrentRoundId(tournamentId)

      , identitySelectableOptions = useMemo(() => (
        identities
          ? Object.keys(identities).map(identity => ({
            value: identity,
            label: `$FVT ${identity}`
          }))
          : []
      ), [ identities ])

      , defaultSelectableIdentity = useMemo(() => (
        identitySelectableOptions && identitySelectableOptions.find(({ value }) => value === selectedIdentity)
      ), [ identitySelectableOptions, selectedIdentity ])

      , updateTimes = useCallback(() => {
        console.log('updateTimes')
        if (tournament && Object.keys(tournament).length > 0) {
          const startTime = tournament.startTime
              , roundLength = tournament.roundLengthSeconds
              , roundEnd = startTime + ((roundId.data) * roundLength)
              , payDay = startTime + ((roundId.data + 1) * roundLength)
              , payDate = new Date(payDay * 1000)
              , roundStart = startTime + ((roundId.data - 1) * roundLength)
              , roundStartDate = new Date(roundStart * 1000)
              , roundEndDate = new Date(roundEnd * 1000)
              , roundProgress = (new Date() / 1000 - roundStart) / (roundEnd - roundStart) * 100

          setRoundEnd(roundEnd * 1000)
          setPayoutDate(payDate)
          setStartDate(roundStartDate)
          setEndDate(roundEndDate)
          setRoundProgress(roundProgress)
        }
      }, [ roundId.data, tournament ])

      , updateTournament = useCallback(async () => {
        console.log('updateTournament')
        try {
          if (tournamentId > 0) {
            const tournament = await getTournament(tournamentId)
            tournament.tokenList = tournament.tokenList.map((row, i) => {
              return {
                ...row,
                index: i + 1,
                icon: logos[row.symbol.toLowerCase()]
              }
            })

            setTournament(tournament)
            setRoundBonus(tournament.tokenRoundBonus)
          }
        } catch (err) {
          console.error(err)
        }
      }, [ roundId ])

      , updateTournamentPrices = useCallback(async () => {
        try {
          if (tournament && tournament.tokenList && tournament.tokenList[0].price === undefined) {
            const prices = await getPrices('live', tournament.tokenList)
            const daysBack = await getDaysBack(tournament, 0)
            console.log({ daysBack })
            tournament.tokenList = tournament.tokenList.map((row, i) => {
              return {
                ...row,
                price: {
                  symbol: '$',
                  value: parseFloat(prices[row.symbol] ? prices[row.symbol].value : prices[row.address].value),
                  difference: (prices[row.symbol] ? prices[row.symbol].difference : prices[row.address]['7d change'] * 100)
                },
                daysBack
              }
            })

            setTournament(tournament)
          }
        } catch (err) {
          console.error(err)
        }
      }, [ tournament ])

      , updateVoiceCredits = useCallback(async () => {
        console.log('updateVoiceCredits')
        try {
          const vc = await getVoiceCredits(tournamentId, selectedIdentity)

          setVPerRound(vc)
        } catch (err) {
          console.error(err)
        }
      }, [ selectedIdentity ])

      , getIdentities = useCallback(async () => {
        console.log('getIdentities')

        try {
          await getIdentitiesForAccount(transferEvent => {
            setIdentities(identities => {
              const identity = transferEvent.tokenId.toString()

              if (!selectedIdentity && identity) {
                setSelectedIdentity(identity)
              }

              return {
                ...(identities || {}),
                [identity]: true
              }
            })
          })
        } catch (err) {
          setGetIdentitiesFailed(true) // TODO consider changing this to a better connection checker
          console.error(err)
        }
      }, [ roundId ])

  function updateRoundId () {
    roundId.refetch()
  }

  function handleIdentityChange ({ value }) {
    setSelectedIdentity(value)
  }

  function handleConnectWallet () {
    connectWallet()
  }

  useEffect(() => {
    const f = async () => {
      if (selectedIdentity) {
        updateVoiceCredits()
        const obj = await getClaimablePowerAll(selectedIdentity, tournamentId)

        console.log('obj', obj)
        if (obj.unclaimedRounds.length > 0) {
          setUpdateRound(obj.unclaimedRounds[0])
        }
      }
    }

    f()
  }, [ selectedIdentity ])

  useEffect(() => {
    updateTournament()
    getIdentities()
  }, [ roundId ])

  useEffect(() => {
    updateTournamentPrices()
  }, [ tournament ])

  useEffect(() => {
    updateTimes()
  }, [ roundId.data, tournament, updateTimes ])

  return (
    <Page
      className={classnames('vote-page', className)}
      leftContent={(
        <Container fluid>
          <Row>
            <Col>
              <h3>Round #{tournament?.currentRoundId}</h3>
              {getIdentitiesFailed
                ? (
                  <Button
                    className="vote-page__connect-wallet-button"
                    variant="link"
                    onClick={handleConnectWallet}
                  >
                    Connect wallet
                  </Button>
                )
                : (
                  <Fragment>
                    <div className="vote-page__caption">Select an identity to vote</div>
                    <Select
                      className="vote-page__identity-control"
                      options={identitySelectableOptions}
                      value={defaultSelectableIdentity}
                      onChange={handleIdentityChange}
                    />
                  </Fragment>
                )
              }
              <div className="vote-page__token-balance">$V Power: {vPerRound}</div>
            </Col>
          </Row>
          <br />
          <Row>
            <Col>
              <div className="vote-page__caption">Tournament details</div>
              <div className="vote-page__progress">
                <Row>
                  <Col>
                    <div className="vote-page__countdown">
                      <Countdown
                        title={''}
                        value={roundEnd}
                        onFinish={updateRoundId}
                      />
                    </div>
                  </Col>
                  <Col>
                    <div className="vote-page__countdown-caption">
                      time to vote close
                    </div>
                  </Col>
                </Row>
                <div>
                  <ProgressBar now={roundProgress} />
                </div>
                <div className="vote-page__round-dates">
                  <Row>
                    <Col>
                      {startDate && formatDateShort(startDate)}
                    </Col>
                    <Col>
                      <div className="vote-page__end-date">
                        {endDate && formatDateShort(endDate)}
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="vote-page__details">
                <BoxedValue label="Reward Pool" value={`${roundBonus} $FVT`}/>
                <BoxedValue label="Market Close" value={payoutDate && formatDate(payoutDate)} />
              </div>
            </Col>
          </Row>
        </Container>
      )}
      rightContent={(
        <Loader
          isLoading={roundId.isLoading}
          error={roundId.error}
        >
          <Form
            className="vote-page__form"
            items={tournament && tournament.tokenList}
            tokensAvailable={vPerRound}
            selectedIdentity={selectedIdentity}
            roundId={roundId.data}
            tournamentId={tournamentId}
            updateRoundId={updateRound}
          />
        </Loader>
      )}
    />
  )
}

VotePage.propTypes = {
  className: PropTypes.string
}

export default memo(VotePage)
