import { memo, Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { Redirect } from 'react-router-dom'
import IdentityCreator from 'components/IdentityCreator'
import ResolveRound from 'components/ResolveRound'
import Page from 'pages/Page'
import { useManagement } from 'utils/management'
import './ManagementPage.scss'

function ManagementPage ({ className, children }) {
  const [ isManagement, setIsManagement ] = useState(true)

  useManagement(({ isManagement }) => {
    setIsManagement(isManagement)
  })

  return (
    <Page
      className={classnames('management-page', className)}
      title="Management"
      leftContent={(
        <Fragment>
          {isManagement
            ? (
              <div>
                <IdentityCreator />
                <ResolveRound />
              </div>
            )
            : (
              <Redirect to="/" />
            )
          }
        </Fragment>
      )}
    />
  )
}

ManagementPage.propTypes = {
  className: PropTypes.string
}

export default memo(ManagementPage)
